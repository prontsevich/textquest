﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuestGame : MonoBehaviour
{
    [SerializeField]
    private string gameTitle;

    [SerializeField]
    TextMeshProUGUI titleTextComponent;

    [SerializeField]
    TextMeshProUGUI storyTextComponent;

    [SerializeField]
    TextMeshProUGUI optionsTextComponent;

    [SerializeField]
    TextMeshProUGUI locationTextComponent;

    [SerializeField]
    TextMeshProUGUI oxygenTextComponent;

    [SerializeField]
    BaseState startState;

    private BaseState currentState;
    private Stack<HistoryItem> history;
    private bool isGameStarted;
    private bool isStateChanged;
    private int oxygenLevel;
    private int selectedEquipment;

    private int oxygenLevel1;
    private int oxygenLevel2;

    private readonly List<KeyCode> _acceptedKeys = new List<KeyCode>
    {
      KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5, KeyCode.Alpha6, KeyCode.Alpha7, KeyCode.Alpha8, KeyCode.Alpha9,
      KeyCode.Keypad1, KeyCode.Keypad2, KeyCode.Keypad3, KeyCode.Keypad4, KeyCode.Keypad5, KeyCode.Keypad6, KeyCode.Keypad7, KeyCode.Keypad8, KeyCode.Keypad9,
      KeyCode.Space, KeyCode.Backspace
    };

    private readonly int _startAlphaKeycode = 49;
    private readonly int _startKeypadKeycode = 257;


    // Start is called before the first frame update
    void Start()
    {
        titleTextComponent.text = gameTitle;
        currentState = startState;
        UpdateText();
        isGameStarted = false;
        history = new Stack<HistoryItem>();
        oxygenLevel1 = 41;
        oxygenLevel2 = 36;
    }

    // Update is called once per frame
    void Update()
    {
        if (isGameStarted)
        {
            isStateChanged = false;
            var keysDict = _acceptedKeys.ToDictionary(x => x, Input.GetKeyDown);

            if (keysDict.Any(x => x.Value))
            {
                var pressedKey = keysDict.First(x => x.Value).Key;

                var nextStates = currentState.GetNextStates();

                switch (pressedKey)
                {
                    // Restart game
                    case KeyCode.Space:
                        Start();
                        break;
                    // Go to previous state
                    case KeyCode.Backspace:
                        if (history.Count > 0)
                        {
                            var historyItem = history.Pop();
                            currentState = historyItem.State;
                            oxygenLevel = historyItem.OxygenLevel;
                            oxygenLevel1 = historyItem.OxygenLevel1;
                            oxygenLevel2 = historyItem.OxygenLevel2;
                            isStateChanged = true;
                        }
                        break;
                    // Go to next state
                    default:
                        var nextIndex = GetSelectedOptionIndex((int)pressedKey);
                        if (nextIndex > -1 && nextStates.Length > nextIndex && nextStates[nextIndex] != null)
                        {
                            if (currentState.GetIsKeepSelectedOption())
                            {
                                selectedEquipment = nextIndex;
                            }

                            history.Push(new HistoryItem { State = currentState, OxygenLevel = oxygenLevel, OxygenLevel1 = oxygenLevel1, OxygenLevel2 = oxygenLevel2});
                            currentState = nextStates[nextIndex];
                            isStateChanged = true;
                            if (currentState.GetType() == typeof(ComplexState))
                            {
                                var currentComplexState = (ComplexState)currentState;
                                if (currentComplexState.CheckOxygen())
                                {
                                    currentComplexState.SetStateIndex(oxygenLevel == oxygenLevel1 ? 0 : oxygenLevel == oxygenLevel2 ? 1 : 2);
                                    UpdateOxygenLevels(5);
                                }
                                else if (currentComplexState.CheckOxygen2())
                                {
                                    currentComplexState.SetStateIndex(oxygenLevel < 13 ? 0 : 1);
                                }
                                else
                                {
                                    currentComplexState.SetStateIndex(selectedEquipment);
                                }
                            }
                        }
                        break;
                }

                if (isStateChanged)
                {
                    UpdateText();
                }
            }
        }

        // Go to start of the story
        if (!isGameStarted && Input.GetKeyDown(KeyCode.Space))
        {
            history.Push(new HistoryItem { State = currentState, OxygenLevel = oxygenLevel, OxygenLevel1 = oxygenLevel1, OxygenLevel2 = oxygenLevel2 });
            currentState = currentState.GetNextStates()[0];
            isGameStarted = true;
            UpdateText();
        }
    }

    private void UpdateText()
    {
        if (currentState.GetType() == typeof(SimpleState) && ((SimpleState)currentState).IsRun())
        {
            storyTextComponent.text = string.Format(currentState.GetStoryText(), oxygenLevel - 1);
            oxygenLevel -= 1;
        }
        else
        {
            storyTextComponent.text = currentState.GetStoryText();
        }

        locationTextComponent.text = currentState.GetLocation();
        optionsTextComponent.text = BuildOptionsText();
        if (currentState.GetOxygenLevel() > 0)
        {
            oxygenLevel = currentState.GetOxygenLevel();
        }

        oxygenTextComponent.text = oxygenLevel > 0 ? $"Кислород: {oxygenLevel}%" : string.Empty;
    }

    private string BuildOptionsText()
    {
        var options = currentState.GetOptions();
        return options.Length == 0
            ? string.Empty
            : string.Concat(options.Select((o, index) => $"{index + 1}. {o}{Environment.NewLine}"));
    }

    private int GetSelectedOptionIndex(int pressedKey)
    {
        if (pressedKey - _startKeypadKeycode >= 0)
            return pressedKey - _startKeypadKeycode;
        if (pressedKey - _startAlphaKeycode >= 0)
            return pressedKey - _startAlphaKeycode;

        return -1;
    }

    private void UpdateOxygenLevels(int amount)
    {
        oxygenLevel1 = oxygenLevel1 - amount;
        oxygenLevel2 = oxygenLevel2 - amount;
    }
}
