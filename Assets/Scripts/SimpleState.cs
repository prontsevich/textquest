﻿using Assets.Scripts;
using UnityEngine;

[CreateAssetMenu(menuName = "Simple State")]
public class SimpleState : BaseState
{
    [SerializeField]
    [TextArea(10, 14)]
    string storyText;

    [SerializeField]
    BaseState[] nextStates;

    [SerializeField]
    string location;

    [SerializeField]
    int oxygenLevel;

    [SerializeField]
    string[] options;

    [SerializeField]
    bool isKeepSelectedOption;

    [SerializeField]
    bool isRun;

    public override string GetStoryText()
    {
        return storyText;
    }

    public override BaseState[] GetNextStates()
    {
        return nextStates;
    }

    public override string GetLocation()
    {
        return location;
    }

    public override int GetOxygenLevel()
    {
        return oxygenLevel;
    }

    public override string[] GetOptions()
    {
        return options;
    }

    public override bool GetIsKeepSelectedOption()
    {
        return isKeepSelectedOption;
    }

    public bool IsRun()
    {
        return isRun;
    }
}
