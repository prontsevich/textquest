﻿using UnityEngine;

namespace Assets.Scripts
{
    [CreateAssetMenu(menuName = "Complex State")]
    public class ComplexState : BaseState
    {
        [SerializeField]
        BaseState[] nextStates;

        [SerializeField]
        bool checkOxygen;

        [SerializeField]
        bool checkOxygen2;

        int stateIndex = 0;

        public void SetStateIndex(int index)
        {
            stateIndex = index;
        }

        public override string GetLocation()
        {
            return nextStates[stateIndex].GetLocation();
        }

        public override BaseState[] GetNextStates()
        {
            return nextStates[stateIndex].GetNextStates();
        }

        public override string[] GetOptions()
        {
            return nextStates[stateIndex].GetOptions();
        }

        public override int GetOxygenLevel()
        {
            return nextStates[stateIndex].GetOxygenLevel();
        }

        public override string GetStoryText()
        {
            return nextStates[stateIndex].GetStoryText();
        }

        public override bool GetIsKeepSelectedOption()
        {
            return nextStates[stateIndex].GetIsKeepSelectedOption();
        }

        public bool CheckOxygen()
        {
            return checkOxygen;
        }

        public bool CheckOxygen2()
        {
            return checkOxygen2;
        }
    }
}
