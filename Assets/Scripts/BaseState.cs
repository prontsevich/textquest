﻿using UnityEngine;

namespace Assets.Scripts
{
    public abstract class BaseState : ScriptableObject
    {
        public abstract string GetStoryText();

        public abstract BaseState[] GetNextStates();

        public abstract string GetLocation();

        public abstract int GetOxygenLevel();

        public abstract string[] GetOptions();

        public abstract bool GetIsKeepSelectedOption();
    }
}
